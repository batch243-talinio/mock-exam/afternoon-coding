let collection = [];


// Write the queue functions below.

function print() {
    // It will show the array
    return collection
}

function enqueue(element) {
    //In this funtion you are going to make an algo that will add an element to the array
    // Mimic the function of push method
    collection.length += 1
    collection[collection.length-1] = element

    return collection
}

function dequeue() {
    // In here you are going to remove the first element in the array
    let temp = [];
    for(let x = collection.length-1; x > 0; x--){
        temp[x-1] = collection[x]
    }
    collection = temp
    return collection
}

function front() {
    // In here, you are going to remove the last element
    let temp;
    for(let x = collection.length-1; x >= 0; x--){
        if(x == 0) temp = collection[x]
    }

    return temp
}

// starting from here, di na pwede gumamit ng .length property
function size() {
     // Number of elements
    let temp = 0;
    for (let i of collection){
        temp++
    }

    return temp 

}

function isEmpty() {
    //it will check whether the function is empty or not
    return (collection == ['']) ? true : false
    

}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};